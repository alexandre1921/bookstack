<?php namespace BookStack\Http\Controllers;

use Activity;
use BookStack\Auth\UserRepo;
use BookStack\Entities\Repos\HeaderRepo;
use BookStack\Exceptions\NotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Views;

class HeaderController extends Controller
{
    protected $pageRepo;
    protected $userRepo;
    public function __construct(HeaderRepo $headerRepo, UserRepo $userRepo)
    {
        $this->headerRepo = $headerRepo;
        $this->userRepo = $userRepo;
        parent::__construct();
    }
    public function index()
    {
        $this->setPageTitle(trans('entities.headers'));
        $list=$this->headerRepo->getRecentlyUpdatedPaginated('header', 20);
        return view('headers.index',['headers' => $list]);
    }

    public function selectIndex()
    {
        $this->setPageTitle(trans('entities.headers'));
        $list=$this->headerRepo->getRecentlyUpdatedPaginated('header', 20);
        return view('headers.select',['headers' => $list]);
    }

    public function view()
    {
        return view('headers.create');
    }

    public function store(Request $request)
    {
        $this->checkPermission('header-create-all');
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $header = $this->headerRepo->createFromInput('header', $request->all());

        return redirect($header->getUrl());
    }

    public function show($headerSlug)
    {
        try { 
            $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        } catch (NotFoundException $e) {
            $header = $this->headerRepo->getHeaderBySlug($headerSlug);
            if ($header === null) {
                throw $e;
            }
        }
        $commentsEnabled = !setting('app-disable-comments');
        if ($commentsEnabled) {
            $header->load(['comments.createdBy']);
        }

        $this->setPageTitle($header->getShortName());
        return view('headers.show',[
            'header' => $header,
            'current' => $header,
            'commentsEnabled' => $commentsEnabled]);         
    }

    public function showDelete($headerSlug)
    {
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('header-delete', $header);
        $this->setPageTitle(trans('entities.headers_delete_named', ['headerName'=>$header->getShortName()]));
        return view('headers.delete', ['header' => $header]);
    }

    public function edit($headerSlug)
    {
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('header-update', $header);
        $this->setPageTitle(trans('entities.headers_editing_named', ['headerName'=>$header->getShortName()]));
        $header->isDraft = true;

        $draftsEnabled = $this->signedIn;
        return view('headers.edit', [
            'header' => $header,
        ]);
    }

    public function update(Request $request, $headerSlug)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255'
        ]);
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('header-update', $header);
        $this->headerRepo->updateHeader($header, $request->all());
        return redirect($header->getUrl());
    }

    public function destroy($headerSlug)
    {
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('header-delete', $header);
        $this->headerRepo->destroyHeader($header);

        session()->flash('success', trans('entities.headers_delete_success'));
        return $this->index();
    }
    
    public function showPermissions($headerSlug)
    {
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('restrictions-manage', $header);
        $roles = $this->userRepo->getRestrictableRoles();
        return view('headers.permissions', [
            'header'  => $header,
            'roles' => $roles
        ]);
    }

    public function permissions($headerSlug, Request $request)
    {
        $header = $this->headerRepo->getHeaderBySlug($headerSlug);
        $this->checkOwnablePermission('restrictions-manage', $header);
        $this->headerRepo->updateEntityPermissionsFromRequest($request, $header);
        session()->flash('success', trans('entities.headers_permissions_success'));
        return redirect($header->getUrl());
    }

}