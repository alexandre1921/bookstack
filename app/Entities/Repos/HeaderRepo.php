<?php namespace BookStack\Entities\Repos;

use BookStack\Entities\Book;
use BookStack\Entities\Chapter;
use BookStack\Entities\Entity;
use BookStack\Entities\Header;
use BookStack\Entities\HeaderRevision;
use Carbon\Carbon;
use DOMDocument;
use DOMElement;
use DOMXPath;

class HeaderRepo extends EntityRepo
{

    /**
     * Search through header revisions and retrieve the last header in the
     * current book that has a slug equal to the one given.
     * @param string $headerSlug
     * @param string $bookSlug
     * @return null|header
     */
    public function getheaderByOldSlug(string $headerSlug)
    {
        $revision = $this->entityProvider->headerRevision->where('slug', '=', $headerSlug)
            ->whereHas('header', function ($query) {
                $this->permissionService->enforceEntityRestrictions('header', $query);
            })
            ->where('type', '=', 'version')
            ->orderBy('created_at', 'desc')
            ->with('header')->first();
        return $revision !== null ? $revision->header : null;
    }

    /**
     * Create a new entity from request input.
     * Used for books and chapters.
     * @param string $type
     * @param array $input
     * @param bool|Book $book
     * @return Entity
     */
    public function createFromInput($type, $input = [], $book = false)
    {
        $isChapter = strtolower($type) === 'chapter';
        $input['html'] = $this->formatHtml($input['html']);
        $entityModel = $this->entityProvider->get($type)->newInstance($input);
        $entityModel->slug = $this->findSuitableSlug($type, $entityModel->name, false, $isChapter ? $book->id : false);
        $entityModel->created_by = user()->id;
        $entityModel->updated_by = user()->id;
        $isChapter ? $book->chapters()->save($entityModel) : $entityModel->save();

        if (isset($input['tags'])) {
            $this->tagRepo->saveTagsToEntity($entityModel, $input['tags']);
        }
        
        $this->permissionService->buildJointPermissionsForEntity($entityModel);
        $this->searchService->indexEntity($entityModel);
        return $entityModel;
    }

    /**
     * Updates a header with any fillable data and saves it into the database.
     * @param header $header
     * @param int $book_id
     * @param array $input
     * @return header
     * @throws \Exception
     */
    public function updateheader(header $header, array $input)
    {
        // Prevent slug being updated if no name change
        if ($header->name !== $input['name']) {
            $header->slug = $this->findSuitableSlug('header', $input['name'], $header->id);
        }

        // Update with new details
        $userId = user()->id;
        $header->fill($input);
        $header->name = $input['name'];
        $header->html = $this->formatHtml($input['html']);
        $header->updated_by = $userId;
        $header->save();

        $this->searchService->indexEntity($header);

        return $header;
    }

    /**
     * Saves a header revision into the system.
     * @param header $header
     * @param null|string $summary
     * @return headerRevision
     * @throws \Exception
     */
    public function saveheaderRevision(header $header, string $summary = null)
    {
        $revision = $this->entityProvider->headerRevision->newInstance($header->toArray());
        if (setting('app-editor') !== 'markdown') {
            $revision->markdown = '';
        }
        $revision->header_id = $header->id;
        $revision->slug = $header->slug;
        $revision->book_slug = $header->book->slug;
        $revision->created_by = user()->id;
        $revision->created_at = $header->updated_at;
        $revision->type = 'version';
        $revision->summary = $summary;
        $revision->revision_number = $header->revision_count;
        $revision->save();

        $revisionLimit = config('app.revision_limit');
        if ($revisionLimit !== false) {
            $revisionsToDelete = $this->entityProvider->headerRevision->where('header_id', '=', $header->id)
                ->orderBy('created_at', 'desc')->skip(intval($revisionLimit))->take(10)->get(['id']);
            if ($revisionsToDelete->count() > 0) {
                $this->entityProvider->headerRevision->whereIn('id', $revisionsToDelete->pluck('id'))->delete();
            }
        }

        return $revision;
    }

    /**
     * Formats a header's html to be tagged correctly within the system.
     * @param string $htmlText
     * @return string
     */
    protected function formatHtml(string $htmlText)
    {
        $htmlText=preg_replace('/(<a.*">|<\/a>)/', '', $htmlText);

        return $htmlText;
    }

    /**
     * Set a unique id on the given DOMElement.
     * A map for existing ID's should be passed in to check for current existence.
     * @param DOMElement $element
     * @param array $idMap
     */
    protected function setUniqueId($element, array &$idMap)
    {
        // if (get_class($element) !== 'DOMElement') {
        //     return;
        // }

        // // Overwrite id if not a BookStack custom id
        // $existingId = $element->getAttribute('id');
        // if (strpos($existingId, 'bkmrk') === 0 && !isset($idMap[$existingId])) {
        //     $idMap[$existingId] = true;
        //     return;
        // }

        // // Create an unique id for the element
        // // Uses the content as a basis to ensure output is the same every time
        // // the same content is passed through.
        // $contentId = 'bkmrk-' . mb_substr(strtolower(preg_replace('/\s+/', '-', trim($element->nodeValue))), 0, 20);
        // $newId = urlencode($contentId);
        // $loopIndex = 0;

        // while (isset($idMap[$newId])) {
        //     $newId = urlencode($contentId . '-' . $loopIndex);
        //     $loopIndex++;
        // }

        // $element->setAttribute('id', $newId);
        // $idMap[$newId] = true;
    }

    /**
     * Get the plain text version of a header's content.
     * @param \BookStack\Entities\header $header
     * @return string
     */
    protected function headerToPlainText(header $header) : string
    {
        $html = $this->renderheader($header, true);
        return strip_tags($html);
    }

    /**
     * Save a header update draft.
     * @param header $header
     * @param array $data
     * @return headerRevision|header
     */
    public function updateheaderDraft(header $header, array $data = [])
    {
        // If the header itself is a draft simply update that
        if ($header->draft) {
            $header->fill($data);
            if (isset($data['html'])) {
                $header->text = $this->headerToPlainText($header);
            }
            $header->save();
            return $header;
        }

        // Otherwise save the data to a revision
        $userId = user()->id;
        $drafts = $this->userUpdateheaderDraftsQuery($header, $userId)->get();

        if ($drafts->count() > 0) {
            $draft = $drafts->first();
        } else {
            $draft = $this->entityProvider->headerRevision->newInstance();
            $draft->header_id = $header->id;
            $draft->slug = $header->slug;
            $draft->book_slug = $header->book->slug;
            $draft->created_by = $userId;
            $draft->type = 'update_draft';
        }

        $draft->fill($data);
        if (setting('app-editor') !== 'markdown') {
            $draft->markdown = '';
        }

        $draft->save();
        return $draft;
    }

    /**
     * Publish a draft header to make it a normal header.
     * Sets the slug and updates the content.
     * @param header $draftheader
     * @param array $input
     * @return header
     * @throws \Exception
     */
    public function publishheaderDraft(header $draftheader, array $input)
    {
        $draftheader->fill($input);

        // Save header tags if present
        if (isset($input['tags'])) {
            $this->tagRepo->saveTagsToEntity($draftheader, $input['tags']);
        }

        $draftheader->slug = $this->findSuitableSlug('header', $draftheader->name, false, $draftheader->book->id);
        $draftheader->html = $this->formatHtml($input['html']);
        $draftheader->text = $this->headerToPlainText($draftheader);
        $draftheader->draft = false;
        $draftheader->revision_count = 1;

        $draftheader->save();
        $this->saveheaderRevision($draftheader, trans('entities.headers_initial_revision'));
        $this->searchService->indexEntity($draftheader);
        return $draftheader;
    }

    /**
     * Destroy a given page along with its dependencies.
     * @param Header $header
     * @throws NotifyException
     * @throws Throwable
     */
    public function destroyHeader(Header $header)
    {
        $this->destroyEntityCommonRelations($header);
        $header->delete();
    }

    /**
     * The base query for getting user update drafts.
     * @param header $header
     * @param $userId
     * @return mixed
     */
    protected function userUpdateheaderDraftsQuery(header $header, int $userId)
    {
        return $this->entityProvider->headerRevision->where('created_by', '=', $userId)
            ->where('type', 'update_draft')
            ->where('header_id', '=', $header->id)
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get the latest updated draft revision for a particular header and user.
     * @param header $header
     * @param $userId
     * @return headerRevision|null
     */
    public function getUserheaderDraft(header $header, int $userId)
    {
        return $this->userUpdateheaderDraftsQuery($header, $userId)->first();
    }

    /**
     * Get the notification message that informs the user that they are editing a draft header.
     * @param headerRevision $draft
     * @return string
     */
    public function getUserheaderDraftMessage(headerRevision $draft)
    {
        $message = trans('entities.headers_editing_draft_notification', ['timeDiff' => $draft->updated_at->diffForHumans()]);
        if ($draft->header->updated_at->timestamp <= $draft->updated_at->timestamp) {
            return $message;
        }
        return $message . "\n" . trans('entities.headers_draft_edited_notification');
    }

    /**
     * A query to check for active update drafts on a particular header.
     * @param header $header
     * @param int $minRange
     * @return mixed
     */
    protected function activeheaderEditingQuery(header $header, int $minRange = null)
    {
        $query = $this->entityProvider->headerRevision->where('type', '=', 'update_draft')
            ->where('header_id', '=', $header->id)
            ->where('updated_at', '>', $header->updated_at)
            ->where('created_by', '!=', user()->id)
            ->with('createdBy');

        if ($minRange !== null) {
            $query = $query->where('updated_at', '>=', Carbon::now()->subMinutes($minRange));
        }

        return $query;
    }

    /**
     * Check if a header is being actively editing.
     * Checks for edits since last header updated.
     * Passing in a minuted range will check for edits
     * within the last x minutes.
     * @param header $header
     * @param int $minRange
     * @return bool
     */
    public function isheaderEditingActive(header $header, int $minRange = null)
    {
        $draftSearch = $this->activeheaderEditingQuery($header, $minRange);
        return $draftSearch->count() > 0;
    }

    /**
     * Get a notification message concerning the editing activity on a particular header.
     * @param header $header
     * @param int $minRange
     * @return string
     */
    public function getheaderEditingActiveMessage(header $header, int $minRange = null)
    {
        $headerDraftEdits = $this->activeheaderEditingQuery($header, $minRange)->get();

        $userMessage = $headerDraftEdits->count() > 1 ? trans('entities.headers_draft_edit_active.start_a', ['count' => $headerDraftEdits->count()]): trans('entities.headers_draft_edit_active.start_b', ['userName' => $headerDraftEdits->first()->createdBy->name]);
        $timeMessage = $minRange === null ? trans('entities.headers_draft_edit_active.time_a') : trans('entities.headers_draft_edit_active.time_b', ['minCount'=>$minRange]);
        return trans('entities.headers_draft_edit_active.message', ['start' => $userMessage, 'time' => $timeMessage]);
    }

    /**
     * Parse the headers on the header to get a navigation menu
     * @param string $headerContent
     * @return array
     */
    public function getheaderNav(string $headerContent)
    {
        if ($headerContent == '') {
            return [];
        }
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML(mb_convert_encoding($headerContent, 'HTML-ENTITIES', 'UTF-8'));
        $xPath = new DOMXPath($doc);
        $headers = $xPath->query("//h1|//h2|//h3|//h4|//h5|//h6");

        if (is_null($headers)) {
            return [];
        }

        $tree = collect($headers)->map(function($header) {
            $text = trim(str_replace("\xc2\xa0", '', $header->nodeValue));
            if (mb_strlen($text) > 30) {
                $text = mb_substr($text, 0, 27) . '...';
            }

            return [
                'nodeName' => strtolower($header->nodeName),
                'level' => intval(str_replace('h', '', $header->nodeName)),
                'link' => '#' . $header->getAttribute('id'),
                'text' => $text,
            ];
        })->filter(function($header) {
            return mb_strlen($header['text']) > 0;
        });

        // Normalise headers if only smaller headers have been used
        $minLevel = $tree->pluck('level')->min();
        $tree = $tree->map(function ($header) use ($minLevel) {
            $header['level'] -= ($minLevel - 2);
            return $header;
        });

        return $tree->toArray();
    }

    /**
     * Restores a revision's content back into a header.
     * @param header $header
     * @param Book $book
     * @param  int $revisionId
     * @return header
     * @throws \Exception
     */
    public function restoreheaderRevision(header $header, Book $book, int $revisionId)
    {
        $header->revision_count++;
        $this->saveheaderRevision($header);
        $revision = $header->revisions()->where('id', '=', $revisionId)->first();
        $header->fill($revision->toArray());
        $header->slug = $this->findSuitableSlug('header', $header->name, $header->id, $book->id);
        $header->text = $this->headerToPlainText($header);
        $header->updated_by = user()->id;
        $header->save();
        $this->searchService->indexEntity($header);
        return $header;
    }

    /**
     * Change the header's parent to the given entity.
     * @param header $header
     * @param Entity $parent
     * @throws \Throwable
     */
    public function changeheaderParent(header $header, Entity $parent)
    {
        $book = $parent->isA('book') ? $parent : $parent->book;
        $header->chapter_id = $parent->isA('chapter') ? $parent->id : 0;
        $header->save();
        if ($header->book->id !== $book->id) {
            $header = $this->changeBook('header', $book->id, $header);
        }
        $header->load('book');
        $this->permissionService->buildJointPermissionsForEntity($book);
    }

    /**
     * Create a copy of a header in a new location with a new name.
     * @param \BookStack\Entities\header $header
     * @param \BookStack\Entities\Entity $newParent
     * @param string $newName
     * @return \BookStack\Entities\header
     * @throws \Throwable
     */
    public function copyheader(header $header, Entity $newParent, string $newName = '')
    {
        $newBook = $newParent->isA('book') ? $newParent : $newParent->book;
        $newChapter = $newParent->isA('chapter') ? $newParent : null;
        $copyheader = $this->getDraftheader($newBook, $newChapter);
        $headerData = $header->getAttributes();

        // Update name
        if (!empty($newName)) {
            $headerData['name'] = $newName;
        }

        // Copy tags from previous header if set
        if ($header->tags) {
            $headerData['tags'] = [];
            foreach ($header->tags as $tag) {
                $headerData['tags'][] = ['name' => $tag->name, 'value' => $tag->value];
            }
        }

        // Set priority
        if ($newParent->isA('chapter')) {
            $headerData['priority'] = $this->getNewChapterPriority($newParent);
        } else {
            $headerData['priority'] = $this->getNewBookPriority($newParent);
        }

        return $this->publishheaderDraft($copyheader, $headerData);
    }
}
