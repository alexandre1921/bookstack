<?php

namespace BookStack\Entities;

use Illuminate\Database\Eloquent\Model;

class Header extends Entity
{
    protected $fillable = ['name', 'slug', 'html', 'height', 'priority'];

    protected $simpleAttributes = ['name', 'id', 'height', 'slug'];

    /**
     * Get the morph class for this model.
     * @return string
     */
    public function getMorphClass()
    {
        return 'BookStack\\Header';
    }

    /**
     * Get the url for this page.
     * @param string|bool $path
     * @return string
     */
    public function getUrl($path = false)
    {
        if ($path !== false) {
            return baseUrl('/headers/view/' . urlencode($this->slug) . $path);
        }
        
        return baseUrl('/headers/view/' . urlencode($this->slug));
    }
}
