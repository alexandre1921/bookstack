<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $book->name }}</title>

    <style>
        @if (!app()->environment('testing'))
        {!! file_get_contents(public_path('/dist/export-styles.css')) !!}
        @endif
        .page-break {
            page-break-after: always;
        }
        .t-pagebreak{
            page-break-after: always;
            border:none;
            visibility: hidden;
        }
        .chapter-hint {
            color: #888;
            margin-top: 32px;
        }
        .chapter-hint + h1 {
            margin-top: 0;
        }
        ul.contents ul li {
            list-style: circle;
        }
        @media screen {
            .page-break {
                border-top: 1px solid #DDD;
            }
        }
    </style>
    @yield('head')
    @include('partials.custom-head')
</head>
<body>

<div class="page-content">
    @foreach($bookChildren as $bookChild)
        @if ($bookChild->priority !== 1)
            <div class="page-break"></div>
        @endif
        @if ($bookChild->put_name)
            <h1 id="{{$bookChild->getType()}}-{{$bookChild->id}}">{{ $bookChild->name }}</h1>
        @else
            <div style="visibility:hidden" id="{{$bookChild->getType()}}-{{$bookChild->id}}">{{ $bookChild->name }}</div>
        @endif
        
        @if($bookChild->isA('chapter'))
            <p>{{ $bookChild->text }}</p>

            @if(count($bookChild->pages) > 0)
                @foreach($bookChild->pages as $page)
                    <div class="page-break"></div>
                    <div class="chapter-hint">{{$bookChild->name}}</div>
                    <h1 id="page-{{$page->id}}">{{ $page->name }}</h1>

                    {!! $page->html !!}
                @endforeach
            @endif
        @else
            {!! $bookChild->html !!}
        @endif

    @endforeach
</div>
</body>
</html>
