@extends('base')

@section('head')
    <script src="{{ baseUrl('/libs/tinymce/tinymce.min.js?ver=4.9.4') }}"></script>
@stop

@section('body-class', 'flexbox')

@section('content')

    <div class="flex-fill flex">
        <form id="header-form" style="width:100%" action ="/headers" method="POST">
        <input type="hidden" name="height" value="a">

            @include('headers.form')
            @include('headers.form-toolbox')
        </form>
    </div>
    
    @include('components.image-manager', ['imageType' => 'gallery', 'uploaded_to' => 'oi'])
    @include('components.code-editor')
    @include('components.entity-selector-popup')

@stop