
<a  class="grid-card" href="./export/pdf/{{ $header->slug }}" data-entity-type="book" data-entity-id="{{$header->id}}">
    <div class="bg-book featured-image-container-wrap">
        @icon('headers')
    </div>
    <div class="grid-card-content">
        <h4>{{ $header->name }}</h4>
        {!!$header->html !!}
    </div>
</a>

