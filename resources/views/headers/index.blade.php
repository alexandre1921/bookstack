@extends('tri-layout')

@section('body')
    @include('headers.list', ['headers' => $headers])
@stop

@section('right')
    <div class="actions mb-xl">
        <h5>{{ trans('common.actions') }}</h5>
        <div class="icon-list text-primary">
            @if($currentUser->can('book-create-all'))
                <a href="{{ baseUrl('/headers/create') }}" class="icon-list-item">
                    <span>@icon('add')</span>
                    <span>{{ trans('entities.headers_create') }}</span>
                </a>
            @endif
            
        </div>
    </div>

@stop