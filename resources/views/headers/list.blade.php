<div class="content-wrap mt-m card">
    <div class="grid half v-center no-row-gap">
        <h1 class="list-heading">{{ trans('entities.headers') }}</h1>
        <div class="text-m-right my-m">

        </div>
    </div>
    @if(count($headers) > 0)
        <div class="grid">
            @foreach($headers as $key => $header)
                @include('headers.grid-item', ['header' => $header])
            @endforeach
        </div>
    @else
        <p class="text-muted">{{ trans('entities.headers_empty') }}</p>
        @if(userCan('headers-create-all'))
            <a href="{{ baseUrl('/headers') }}" class="text-pos">@icon('edit'){{ trans('entities.create_now') }}</a>
        @endif
    @endif
</div>