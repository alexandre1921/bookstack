@extends('tri-layout')

@section('body')

    <div class="my-s">
            @include('partials.breadcrumbs', ['crumbs' => [
                null,
                $header,
            ]])
    </div>

    <div class="content-wrap card">
        <div class="page-content flex" page-display="{{ $header->id }}">

            <div class="pointer-container" id="pointer">
                <div class="pointer anim {{ userCan('page-update', $header) ? 'is-page-editable' : ''}}" >
                
                    <span class="icon text-primary">@icon('link') @icon('include', ['style' => 'display:none;'])</span>
                    <span class="input-group">
                    <input readonly="readonly" type="text" id="pointer-url" placeholder="url">
                    <button class="button icon" data-clipboard-target="#pointer-url" type="button" title="{{ trans('entities.pages_copy_link') }}">@icon('copy')</button>
                    </span>
                    @if(userCan('page-update', $header))
                        <a href="{{ $header->getUrl('/edit') }}" id="pointer-edit" data-edit-href="{{ $header->getUrl('/edit') }}"
                           class="button icon heading-edit-icon" title="{{ trans('entities.pages_edit_content_link')}}">@icon('edit')</a>
                    @endif
                </div>
            </div>
            @include('headers.header-display')
        </div>
    </div>

@stop


@section('right')
    <div id="page-details" class="entity-details mb-xl">
        <h5>{{ trans('common.details') }}</h5>
        <div class="body text-small blended-links">
            @include('partials.entity-meta', ['entity' => $header])


            @if($header->restricted)
                <div class="active-restriction">
                    @if(userCan('restrictions-manage', $header))
                        <a href="{{ $header->getUrl('/permissions') }}">@icon('lock'){{ trans('entities.pages_permissions_active') }}</a>
                    @else
                        @icon('lock'){{ trans('entities.pages_permissions_active') }}
                    @endif
                </div>
            @endif
        </div>
    </div>

    <div class="actions mb-xl">
        <h5>Actions</h5>

        <div class="icon-list text-primary">

            {{--User Actions--}}
            @if(userCan('page-update', $header))
                <a href="{{ $header->getUrl('/edit') }}" class="icon-list-item">
                    <span>@icon('edit')</span>
                    <span>{{ trans('common.edit') }}</span>
                </a>
            @endif
            @if(userCan('restrictions-manage', $header))
                <a href="{{ $header->getUrl('/permissions') }}" class="icon-list-item">
                    <span>@icon('lock')</span>
                    <span>{{ trans('entities.permissions') }}</span>
                </a>
            @endif
            @if(userCan('page-delete', $header))
                <a href="{{ $header->getUrl('/delete') }}" class="icon-list-item">
                    <span>@icon('delete')</span>
                    <span>{{ trans('common.delete') }}</span>
                </a>
            @endif
        </div>

    </div>
@stop
