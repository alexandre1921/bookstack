@extends('base')

@section('head')
    <script src="{{ baseUrl('/libs/tinymce/tinymce.min.js?ver=4.9.4') }}"></script>
@stop

@section('body-class', 'flexbox')

@section('content')

    <div class="flex-fill flex">
        <form id="header-form" style="width:100%" action ="/headers/view/{{ $header->slug }}" data-header-id="{{ $header->id }}" method="POST">
            <input type="hidden" name="height" value="0">
            @if(!isset($isDraft))
                <input type="hidden" name="_method" value="PUT">
            @endif
            @include('headers.form', ['model' => $header])
            @include('headers.form-toolbox')
        </form>
    </div>
    
    @include('components.image-manager', ['imageType' => 'gallery', 'uploaded_to' => $header->id])
    @include('components.code-editor')
    @include('components.entity-selector-popup')

@stop