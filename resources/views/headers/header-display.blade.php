<div dir="auto">
    @if ($header->put_name)
        <h1 class="break-text" v-pre id="bkmrk-page-title">{{$header->name}}</h1>
    @endif

    <div style="clear:left;"></div>

    @if (isset($diff) && $diff)
        {!! $diff !!}
    @else
        {!! isset($header->renderedHTML) ? $header->renderedHTML : $header->html !!}
    @endif
</div>