<a class="book entity-list-item" data-entity-type="book" data-entity-id="{{$header->id}}">
    <div class="entity-list-item-image bg-book">
        @icon('headers')
    </div>
    <div class="content">
        <h4 class="entity-list-item-name break-text">{{ $header->name }}</h4>
    </div>
</a>

<!-- <p>@icon('star')<span title="{{$header->created_at->toDayDateTimeString()}}">{{ trans('entities.meta_created', ['timeLength' => $header->created_at->diffForHumans()]) }}</span></p>
        <p>@icon('edit')<span title="{{ $header->updated_at->toDayDateTimeString() }}">{{ trans('entities.meta_updated', ['timeLength' => $header->updated_at->diffForHumans()]) }}</span></p> -->