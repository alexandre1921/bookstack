@extends('simple-layout')

@section('body')

    <div class="container">

        <div class="my-s">
            @include('partials.breadcrumbs', ['crumbs' => [
                $header,
                $header->getUrl('/permissions') => [
                    'text' => trans('entities.headers_permissions'),
                    'icon' => 'lock',
                ]
            ]])
        </div>

        <div class="card content-wrap">
            <h1 class="list-heading">{{ trans('entities.headers_permissions') }}</h1>
            @include('form.entity-permissions', ['model' => $header])
        </div>
    </div>

@stop
