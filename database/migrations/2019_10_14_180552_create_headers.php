<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeaders extends Migration
{
 /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->indexed();
            $table->longText('html');
            $table->integer('priority');
            $table->integer('height');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->boolean('restricted')->default(false);
            $table->nullableTimestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('headers');
    }
}
